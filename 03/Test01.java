import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Test01
{  
  public static void main (String[] args) 
{
  System.out.println("Starting");
  System.out.println("\n\n***** MySQL JDBC Connection Testing *****");
  Connection conn = null;
 
  try 
  {
    Class.forName ("org.mariadb.jdbc.Driver");
    String userName = "root";
    String password = "root";
    String url = "jdbc:mariadb://127.0.0.1/root";        
    conn = DriverManager.getConnection (url, userName, password);
    System.out.println ("\nDatabase Connection Established...");
  }
  catch (Exception ex) 
  {
    System.err.println ("Cannot connect to database server");
    ex.printStackTrace();
  }      

  try 
  {
    String query = "SELECT * FROM names";
    // create the java statement
    Statement st = conn.createStatement();
    // execute the query, and get a java resultset
    ResultSet rs = st.executeQuery(query);
    // iterate through the java resultset
    while (rs.next()) {
      String firstName = rs.getString("firstname");
      String lastName = rs.getString("lastname");
      // print the results
      System.out.format("%s, %s \n", firstName, lastName);
    }
    st.close();
  }
  catch (Exception e) 
  {
    System.err.println("Got an exception! ");
    System.err.println(e.getMessage());
  }

  if (conn != null)
  {
    try
    {
      System.out.println("\n***** Let terminate the Connection *****");
			conn.close ();					   
      System.out.println ("\nDatabase connection terminated...");
    }
    catch (Exception ex)
	{
		System.out.println ("Error in connection termination!");
	}
  }
  System.out.println("Stopping");
}
}


